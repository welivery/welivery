<?php
/**
 *
 * @name Ids\Welivery\Api
 *
 * @description Welivery API v4.0
 *
 */
namespace Ids\Welivery;
class Api
{
    /**
     *
     * @const CREATE_RESOURCE API create resource
     *
     */
    const CREATE_RESOURCE = 'delivery_create';

    /**
     *
     * @const UPDATE_RESOURCE API update resource
     *
     */
    const UPDATE_RESOURCE = 'delivery_update';

    /**
     *
     * @const STATUS_RESOURCE API status resource
     *
     */
    const STATUS_RESOURCE = 'delivery_status';

    /**
     *
     * @const CANCEL_RESOURCE API cancel resource
     *
     */
    const CANCEL_RESOURCE = 'delivery_cancel';

    /**
     *
     * @const PRICE_RESOURCE API price resource
     *
     */
    const PRICE_RESOURCE  = 'delivery_price';

    /**
     *
     * @const GET_RESOURCE API get resource
     *
     */
    const GET_RESOURCE    = 'delivery_get';

    /**
     *
     * @const LIST_RESOURCE API list resource
     *
     */
    const LIST_RESOURCE   = 'delivery_list';

    /**
     *
     * @var string $_url API end point
     *
     */
    private $_url;

    /**
     *
     * @var string $_apiKey API user authentication
     *
     */
    private $_apiKey;

    /**
     *
     * @var string $_apiSecret API password authentication
     *
     */
    private $_apiSecret;

    /**
     *
     * Api constructor.
     *
     * @param string $url API end point (without '/' at the bottom)
     * @param string $apiKey API user authentication
     * @param string $apiSecret API password authentication
     *
     */
    public function __construct($url, $apiKey, $apiSecret)
    {
        $this->_url       = $url;
        $this->_apiKey    = $apiKey;
        $this->_apiSecret = $apiSecret;
    }

    /**
     *
     * Create shipment
     *
     * @param array $data Data to create the shipment
     *
     * @note See Welivery API 4.0 Documentation
     *
     * @return array
     *
     */
    public function createShipment($data)
    {
        return $this->_doRequest($this->_getUri(self::CREATE_RESOURCE), \Zend\Http\Request::METHOD_POST, $data);
    }

    /**
     *
     * Update shipment
     *
     * @param array $data Data to update the shipment
     *
     * @note See Welivery API 4.0 Documentation
     *
     * @return array
     *
     */
    public function updateShipment($data)
    {
        return $this->_doRequest($this->_getUri(self::UPDATE_RESOURCE), \Zend\Http\Request::METHOD_POST, $data);
    }

    /**
     *
     * Cancel shipment
     *
     * @param int $id Shipment ID
     *
     * @note See Welivery API 4.0 Documentation
     *
     * @return array
     *
     */
    public function cancelShipment($id)
    {
        return $this->_doRequest($this->_getUri(self::CANCEL_RESOURCE), \Zend\Http\Request::METHOD_POST, array('Id' => $id));
    }

    /**
     *
     * Get shipment status
     *
     * @param int $id Shipment ID
     *
     * @note See Welivery API 4.0 Documentation
     *
     * @return array
     *
     */
    public function getShipmentStatus($id)
    {
        return $this->_doRequest($this->_getUri(self::STATUS_RESOURCE), \Zend\Http\Request::METHOD_GET, array('Id' => $id));
    }

    /**
     *
     * Get postal code price
     *
     * @param string $postCode Postal code to get its price
     *
     * @note See Welivery API 4.0 Documentation
     *
     * @return array
     *
     */
    public function getShipmentPrice($postCode)
    {
        return $this->_doRequest($this->_getUri(self::PRICE_RESOURCE), \Zend\Http\Request::METHOD_GET, array('PostalCode' => $postCode));
    }

    /**
     *
     * Get shipment info
     *
     * @param int $id Shipment ID
     *
     * @note See Welivery API 4.0 Documentation
     *
     * @return array
     *
     */
    public function getShipment($id)
    {
        return $this->_doRequest($this->_getUri(self::GET_RESOURCE), \Zend\Http\Request::METHOD_GET, array('Id' => $id));
    }

    /**
     *
     * List all shipments
     *
     * @return array
     *
     */
    public function listShipments()
    {
        return $this->_doRequest($this->_getUri(self::LIST_RESOURCE), \Zend\Http\Request::METHOD_GET, array());
    }

    /**
     *
     * Do HTTP Request
     *
     * @param string $uri API URI to request
     * @param string $method HTTP method
     * @param array  $data Data to send
     *
     * @return array The JSON response as associative array
     *
     */
    protected function _doRequest($uri, $method, $data)
    {
        $client = new \Zend\Http\Client();

        $client->setAuth($this->_apiKey, $this->_apiSecret, \Zend\Http\Client::AUTH_BASIC);
        $client->setUri($uri);
        $client->setMethod($method);

        if($method  == \Zend\Http\Request::METHOD_GET) {
            $client->setParameterGet($data);
        }

        if($method  == \Zend\Http\Request::METHOD_POST) {
            $client->setParameterPost($data);
        }

        $response = $client->send();
        return json_decode($response->getBody(), true);
    }

    /**
     *
     * Return the URI to request
     *
     * @param string $resource API resource
     *
     * @return string
     *
     */
    private function _getUri($resource)
    {
        return $this->_url . '/' . $resource;
    }
}